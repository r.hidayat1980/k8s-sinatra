# How to deploy this application on top kubernetes

requirements:

* assuming kubernetes cluster already running
* assuming namespaces already created (spots-stg)
* assuming tiller pernamespaces already configured and we have an access to that namespace (spots-stg)
* assuming docker hub is used for storing docker images

on root application directory

```bash
docker login
draft config set registry rhidayat1980
draft up --tiller-namespace spots-stg

```
