FROM ruby
ENV PORT 3000
EXPOSE 3000

WORKDIR /app

ADD Gemfile  /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock
RUN gem update --system
RUN gem install bundler
RUN bundle install
ADD . /app/

CMD ["ruby", "helloworld.rb"]
